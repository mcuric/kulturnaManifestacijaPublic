package manifestacija.model;

public class Grad {
	
	protected int id;
	protected String naziv;
	protected String ptt;
	
	public Grad () {}

	public Grad(int id, String naziv, String ptt) {
		this.id = id;
		this.naziv = naziv;
		this.ptt = ptt;
	}
	
	public String toSting(){
		String ispis = "Grad sa ID-jem: " + id + " ima naziv " + naziv + " sa poštaniskim brojem: " +ptt;
		return ispis;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grad other = (Grad) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPtt() {
		return ptt;
	}

	public void setPtt(String ptt) {
		this.ptt = ptt;
	}
	
	
}
