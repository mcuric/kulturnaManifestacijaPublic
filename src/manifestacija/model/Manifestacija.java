package manifestacija.model;

public class Manifestacija {

	protected int id;
	protected String naziv;
	protected int brojPosetinalaca;
	
	
	public Manifestacija() {
		
	}

	@Override
	public String toString() {
		return "Manifestacija [id=" + id + ", naziv=" + naziv
				+ ", brojPosetinalaca=" + brojPosetinalaca + "]";
	}

	public Manifestacija(int id, String naziv, int brojPosetinalaca) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojPosetinalaca = brojPosetinalaca;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getBrojPosetinalaca() {
		return brojPosetinalaca;
	}
	public void setBrojPosetinalaca(int brojPosetinalaca) {
		this.brojPosetinalaca = brojPosetinalaca;
	}
	
	
	
	
}
