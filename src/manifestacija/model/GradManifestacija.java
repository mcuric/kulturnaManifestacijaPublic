package manifestacija.model;

import java.util.ArrayList;

public class GradManifestacija {
	
	ArrayList <Manifestacija> sveManifestacije = new ArrayList<>();
	ArrayList <Grad> sviGradovi = new ArrayList<>();
	
	
	public GradManifestacija() {
		
	}
	@Override
	public String toString() {
		return "GradManifestacija [sveManifestacije=" + sveManifestacije + ", sviGradovi=" + sviGradovi + "]";
	}
	public GradManifestacija(ArrayList<Manifestacija> sveManifestacije, ArrayList<Grad> sviGradovi) {
		this.sveManifestacije = sveManifestacije;
		this.sviGradovi = sviGradovi;
	}
	
	public ArrayList<Grad> getSviGradovi() {
		return sviGradovi;
	}

	public void setSviGradovi(ArrayList<Grad> sviGradovi) {
		this.sviGradovi = sviGradovi;
	}

	

	public ArrayList<Manifestacija> getSveManifestacije() {
		return sveManifestacije;
	}
	public void setSveManifestacije(ArrayList<Manifestacija> sveManifestacije) {
		this.sveManifestacije = sveManifestacije;
	}
	
	
}
