drop database if exists kulturnamanifestacija;
CREATE DATABASE kulturnamanifestacija;
USE kulturnamanifestacija;

create table grad (
	id int not null auto_increment,
    naziv varchar(35) not null,
    ptt varchar(35) not null,
    primary key(id)
);

create table manifestacija (
	id int not null auto_increment,
    naziv varchar(35),
    broj_posetilaca int,
    primary key(id)
);

-- Vezna tabela vise na vise
create table gradmanifestacija (
	grad_id int not null,
    manifestacija_id int not null,
    foreign key (grad_id) references grad(id),
    foreign key (manifestacija_id) references manifestacija(id)
);